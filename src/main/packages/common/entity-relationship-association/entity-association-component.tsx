/*
  This file handles generating the Relationship lines for Entity Relationship diagram elements
*/

import React, { FunctionComponent } from 'react';
import { Direction, IUMLElementPort } from '../../../services/uml-element/uml-element-port';
import { Point } from '../../../utils/geometry/point';
import { ClassRelationshipType } from '../../uml-class-diagram';
import { EntityAssociation } from './entity-association';
import { UMLRelationshipType } from '../../uml-relationship-type';
import { ThemedPath, ThemedPathContrast, ThemedPolyline } from '../../../components/theme/themedComponents';

// Creates the marker for the specific endpoint
// ThemedPath is where the actual endpoint is rendered, it is created using the "d" argument
// Documentation for "d" argument: https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/d#path_commands
const Marker = {
  Default: (id: string, color?: string, scale: number = 1.0, isSource: boolean = false) => (
    <marker
      id={isSource ? `source-${id}` : `target-${id}`}
      viewBox="0 0 30 30"
      markerWidth="30"
      markerHeight="30"
      refX="30"
      refY="15"
      orient={isSource ? 'auto-start-reverse' : 'auto'}
      markerUnits="strokeWidth"
    >
      <ThemedPath />
    </marker>
  ),
  OneTo: (id: string, color?: string, scale: number = 1.0, isSource: boolean = false) => (
    <marker
      id={isSource ? `source-${id}` : `target-${id}`}
      viewBox="0 0 30 30"
      markerWidth="30"
      markerHeight="30"
      refX="30"
      refY="15"
      orient={isSource ? 'auto-start-reverse' : 'auto'}
      markerUnits="strokeWidth"
    >
      <ThemedPath d="M 22 5 V 25 Z" fillColor="none" strokeColor={color} />
    </marker>
  ),
  OneAndOnlyOneTo: (id: string, color?: string, scale: number = 1.0, isSource: boolean = false) => (
    <marker
      id={isSource ? `source-${id}` : `target-${id}`}
      viewBox="0 0 30 30"
      markerWidth="30"
      markerHeight="30"
      refX="30"
      refY="15"
      orient={isSource ? 'auto-start-reverse' : 'auto'}
      markerUnits="strokeWidth"
    >
      <ThemedPath d="M 22 5 V 25 Z M 13 5 V 25 Z" fillColor="none" strokeColor={color} />
    </marker>
  ),
  ZeroOrOneTo: (id: string, color?: string, scale: number = 1.0, isSource: boolean = false) => (
    <marker
      id={isSource ? `source-${id}` : `target-${id}`}
      style={{ transform: `scale(${scale})` }}
      viewBox="0 0 30 30"
      markerWidth="30"
      markerHeight="30"
      refX="30"
      refY="15"
      orient={isSource ? 'auto-start-reverse' : 'auto'}
      markerUnits="strokeWidth"
    >
      <ThemedPath d="M 22 5 V 25 Z M 0 15 A 1 1 0 0 0 15 15 A 1 1 0 0 0 0 15 Z" fillColor={color} strokeColor={color} />
    </marker>
  ),
  ManyTo: (id: string, color?: string, scale: number = 1.0, isSource: boolean = false) => (
    <marker
      id={isSource ? `source-${id}` : `target-${id}`}
      style={{ transform: `scale(${scale})` }}
      viewBox="0 0 30 30"
      markerWidth="30"
      markerHeight="30"
      refX="30"
      refY="15"
      orient={isSource ? 'auto-start-reverse' : 'auto'}
      markerUnits="strokeWidth"
    >
      <ThemedPath d="M 29 1 L 1 15 Z M 29 15 L 1 15 Z M 29 29 L 1 15 Z" fillColor={color} strokeColor={color} />
    </marker>
  ),
  OneOrManyTo: (id: string, color?: string, scale: number = 1.0, isSource: boolean = false) => (
    <marker
      id={isSource ? `source-${id}` : `target-${id}`}
      style={{ transform: `scale(${scale})` }}
      viewBox="0 0 30 30"
      markerWidth="30"
      markerHeight="30"
      refX="30"
      refY="15"
      orient={isSource ? 'auto-start-reverse' : 'auto'}
      markerUnits="strokeWidth"
    >
      <ThemedPath
        d="M 29 1 L 1 15 Z M 29 15 L 1 15 Z M 29 29 L 1 15 Z M 0 1 V 29 Z"
        fillColor={color}
        strokeColor={color}
      />
    </marker>
  ),
  ZeroOrManyTo: (id: string, color?: string, scale: number = 1.0, isSource: boolean = false) => (
    <marker
      id={isSource ? `source-${id}` : `target-${id}`}
      style={{ transform: `scale(${scale})` }}
      viewBox="0 0 30 30"
      markerWidth="30"
      markerHeight="30"
      refX="30"
      refY="15"
      orient={isSource ? 'auto-start-reverse' : 'auto'}
      markerUnits="strokeWidth"
    >
      <ThemedPath
        d="M 29 1 L 15 15 Z M 29 15 L 15 15 Z M 29 29 L 15 15 Z V 29 M 0 15 A 1 1 0 0 0 15 15 A 1 1 0 0 0 0 15 Z"
        fillColor={color}
        strokeColor={color}
      />
    </marker>
  ),
};

// Decides where to render text for the relationship line (TODO: DECIDE IF NEEDED IN ER DIAGRAM)
export const layoutTextForEntityAssociation = (location: IUMLElementPort['direction'], position: 'TOP' | 'BOTTOM') => {
  switch (location) {
    case Direction.Up:
    case Direction.Topright:
    case Direction.Topleft:
      return {
        dx: position === 'TOP' ? -5 : 5,
        textAnchor: position === 'TOP' ? 'end' : 'start',
      };
    case Direction.Right:
    case Direction.Upright:
    case Direction.Downright:
      return {
        dy: position === 'TOP' ? -10 : 21,
        textAnchor: 'start',
      };
    case Direction.Down:
    case Direction.Bottomright:
    case Direction.Bottomleft:
      return {
        dx: position === 'TOP' ? -5 : 5,
        dy: 10,
        textAnchor: position === 'TOP' ? 'end' : 'start',
      };
    case Direction.Left:
    case Direction.Upleft:
    case Direction.Downleft:
      return {
        dy: position === 'TOP' ? -10 : 21,
        textAnchor: 'end',
      };
  }
};

// Decides where to position text for the relationship line (TODO: DECIDE IF RELEVANT)
export const computeTextPositionForEntityAssociation = (alignmentPath: Point[], hasMarker: boolean = false): Point => {
  const distance = hasMarker ? 31 : 8;
  if (alignmentPath.length < 2) return new Point();
  const vector = alignmentPath[1].subtract(alignmentPath[0]);
  return alignmentPath[0].add(vector.normalize().scale(distance));
};

// Generates the marker for the relationship line (TODO: CHANGE TO ER TYPES)
export const getMarkerForTypeForEntityAssociation = (relationshipType: string) => {
  return ((type) => {
    switch (relationshipType) {
      case 'Default':
        return Marker.Default;
      case 'OneTo':
        return Marker.OneTo;
      case 'ZeroOrOneTo':
        return Marker.ZeroOrOneTo;
      case 'OneAndOnlyOneTo':
        return Marker.OneAndOnlyOneTo;
      case 'ManyTo':
        return Marker.ManyTo;
      case 'OneOrManyTo':
        return Marker.OneOrManyTo;
      case 'ZeroOrManyTo':
        return Marker.ZeroOrManyTo;
    }
  })(relationshipType);
};

// Renders our relationship line
export const EntityAssociationComponent: FunctionComponent<Props> = ({ element, scale }) => {
  const sourceMarker = getMarkerForTypeForEntityAssociation(element.source.role);
  const targetMarker = getMarkerForTypeForEntityAssociation(element.target.role);

  const path = element.path.map((point) => new Point(point.x, point.y));
  const source: Point = computeTextPositionForEntityAssociation(path, !!targetMarker);
  const target: Point = computeTextPositionForEntityAssociation(path.reverse(), !!sourceMarker);
  const id = `marker-${element.id}`;

  const textFill = element.textColor ? { fill: element.textColor } : {};
  return (
    <g>
      {targetMarker && targetMarker(id, element.strokeColor, scale, false)}
      {sourceMarker && sourceMarker(id, element.strokeColor, scale, true)}
      <ThemedPolyline
        points={element.path.map((point) => `${point.x} ${point.y}`).join(',')}
        strokeColor={element.strokeColor}
        fillColor="none"
        strokeWidth={1}
        markerStart={`url(#source-${id})`}
        markerEnd={`url(#target-${id})`}
      />
    </g>
  );
};

interface Props {
  element: EntityAssociation;
  scale: number;
}
