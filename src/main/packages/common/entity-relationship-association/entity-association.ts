import { DeepPartial } from 'redux';
import { ILayer } from '../../../services/layouter/layer';
import { ILayoutable } from '../../../services/layouter/layoutable';
import { UMLElement } from '../../../services/uml-element/uml-element';
import { Direction, IUMLElementPort } from '../../../services/uml-element/uml-element-port';
import { IUMLRelationship, UMLRelationship } from '../../../services/uml-relationship/uml-relationship';
import { UMLRelationships } from '../../uml-relationships';
import { assign } from '../../../utils/fx/assign';
import { computeBoundingBoxForElements, IBoundary } from '../../../utils/geometry/boundary';
import {
  computeTextPositionForEntityAssociation,
  getMarkerForTypeForEntityAssociation,
  layoutTextForEntityAssociation,
} from './entity-association-component';
import { Text } from '../../../utils/svg/text';
import { Point } from '../../../utils/geometry/point';
import { EntityRelationshipEndpointType } from '../../entity-relationship/index';
import { DefaultUMLRelationshipType, UMLRelationshipType } from '../../uml-relationship-type';
import { IUMLAssociation } from '../../common/uml-association/uml-association';

// Creates an interface for the user to use when rendering the different elements
// Same as UMLAssociation, but added additional types to source and target to allow for manipulation of individual endpoints
export interface IEntityAssociation extends IUMLAssociation {
  source: IUMLElementPort & {
    multiplicity: string;
    role: string;
  };
  target: IUMLElementPort & {
    multiplicity: string;
    role: string;
  };
}

const textWithLayoutPropertiesToBounds = (
  layer: ILayer,
  anchor: Point,
  text: string,
  layoutOptions: { dx?: number; dy?: number; textAnchor: string },
): { bounds: IBoundary } => {
  const textSize = Text.size(layer, text, { textAnchor: layoutOptions.textAnchor });
  return {
    bounds: {
      x:
        anchor.x +
        (layoutOptions.textAnchor === 'end' ? -textSize.width : 0) +
        (layoutOptions.dx ? layoutOptions.dx : 0),
      y: anchor.y + (layoutOptions.dy ? layoutOptions.dy : 0),
      width: textSize.width,
      height: textSize.height,
    },
  };
};

// Establishes default creation of an entity relationship line
export abstract class EntityAssociation extends UMLRelationship implements IEntityAssociation {
  source: IEntityAssociation['source'] = {
    direction: Direction.Up,
    element: '',
    multiplicity: '',
    role: EntityRelationshipEndpointType.Default,
  };
  target: IEntityAssociation['target'] = {
    direction: Direction.Up,
    element: '',
    multiplicity: '',
    role: EntityRelationshipEndpointType.Default,
  };

  constructor(values?: DeepPartial<IEntityAssociation>) {
    super();
    assign<IEntityAssociation>(this, values);
  }

  // Renders our line component based on the responses from the entity-association-component
  render(canvas: ILayer, source?: UMLElement, target?: UMLElement): ILayoutable[] {
    super.render(canvas, source, target);

    // TODO: hacky way of computing bounding box, should follow layoutable (make connection text layoutable)
    const pathBounds = this.bounds;

    // multiplicity
    const sourceMultiplicity = layoutTextForEntityAssociation(this.source.direction, 'BOTTOM');
    const targetMultiplicity = layoutTextForEntityAssociation(this.target.direction, 'BOTTOM');

    // roles
    const sourceRole = layoutTextForEntityAssociation(this.source.direction, 'TOP');
    const targetRole = layoutTextForEntityAssociation(this.target.direction, 'TOP');

    // calculate anchor points
    // anchor point = endOfPath + this.position
    const sourceMarker = getMarkerForTypeForEntityAssociation(this.source.role);
    const targetMarker = getMarkerForTypeForEntityAssociation(this.target.role);
    const path = this.path.map((point) => new Point(point.x, point.y));
    const sourceAnchor: Point = computeTextPositionForEntityAssociation(path, !!targetMarker).add(
      this.bounds.x,
      this.bounds.y,
    );
    const targetAnchor: Point = computeTextPositionForEntityAssociation(path.reverse(), !!sourceMarker).add(
      this.bounds.x,
      this.bounds.y,
    );

    const boundingElements = [
      textWithLayoutPropertiesToBounds(canvas, sourceAnchor, this.source.multiplicity, sourceMultiplicity),
      textWithLayoutPropertiesToBounds(canvas, targetAnchor, this.target.multiplicity, targetMultiplicity),
      textWithLayoutPropertiesToBounds(canvas, sourceAnchor, this.source.role, sourceRole),
      textWithLayoutPropertiesToBounds(canvas, targetAnchor, this.target.role, targetRole),
    ];

    this.bounds = computeBoundingBoxForElements([this, ...boundingElements]);

    const horizontalTranslation = pathBounds.x - this.bounds.x;
    const verticalTranslation = pathBounds.y - this.bounds.y;

    // translation of path points, because they are relative to their own bounding box
    // the bounding may be different now -> translation to correct this
    this.path.forEach((point) => {
      point.x += horizontalTranslation;
      point.y += verticalTranslation;
    });

    return [this];
  }
}
