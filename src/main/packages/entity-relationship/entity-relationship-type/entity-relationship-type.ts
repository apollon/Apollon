import { EntityRelationshipType } from '..';
import { EntityAssociation } from '../../common/entity-relationship-association/entity-association';

// Handles letting the rendering know the type of the relationship is EntityRelationship
export class ERType extends EntityAssociation {
  type = EntityRelationshipType.EntityRelationship;
}
