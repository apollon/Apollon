import { UMLElement } from '../../services/uml-element/uml-element';
import { ILayer } from '../../services/layouter/layer';
import { ComposePreview } from '../compose-preview';
import { Entity } from './entity/entity';
import { EntityFieldType } from './entity-field/entity-field-type';
import { EntityFieldKeys } from './entity-field/entity-field-keys';
import { EntityFieldName } from './entity-field/entity-field-name';
import { computeDimension } from '../../utils/geometry/boundary';

export const composeEntityRelationshipPreview: ComposePreview = (
  layer: ILayer,
  translate: (id: string) => string,
  scale: number,
): UMLElement[] => {
  const elements: UMLElement[] = [];

  // Entity
  const entity = new Entity({
    name: 'Entity',
    bounds: {
      x: 0,
      y: 0,
      width: computeDimension(scale, 200),
      height: computeDimension(scale, 40),
    },
  });

  const fieldKeys = new EntityFieldKeys({
    name: 'PK',
    owner: entity.id,
    bounds: {
      x: 0,
      y: 0,
      width: 0,
      height: computeDimension(scale, 40),
    },
  });

  const fieldName = new EntityFieldName({
    name: 'Field',
    owner: entity.id,
    bounds: {
      x: 0,
      y: 0,
      width: 0,
      height: computeDimension(scale, 40),
    },
  });

  const fieldType = new EntityFieldType({
    name: 'TYPE',
    owner: entity.id,
    bounds: {
      x: 0,
      y: 0,
      width: 0,
      height: computeDimension(scale, 40),
    },
  });

  entity.ownedElements = [fieldKeys.id, fieldName.id, fieldType.id];

  elements.push(...(entity.render(layer, [fieldKeys, fieldName, fieldType]) as UMLElement[]));

  return elements;
};
