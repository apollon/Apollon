import { EntityRelationshipElementType } from '..';
import { UMLClassifierMember } from '../../common/uml-classifier/uml-classifier-member';

export class EntityFieldKeys extends UMLClassifierMember {
  type = EntityRelationshipElementType.EntityFieldKeys;
}
