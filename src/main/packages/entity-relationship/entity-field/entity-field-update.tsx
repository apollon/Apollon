import React, { useState } from 'react';
import styled from 'styled-components';
import { Button } from '../../../components/controls/button/button';
import { ColorButton } from '../../../components/controls/color-button/color-button';
import { TrashIcon } from '../../../components/controls/icon/trash';
import { Text } from '../../../components/controls/text/text';
import { Textfield } from '../../../components/controls/textfield/textfield';
import { StylePane } from '../../../components/style-pane/style-pane';
import { IUMLElement, UMLElement } from '../../../services/uml-element/uml-element';
import { EntityFieldType } from './entity-field-type';
import { EntityFieldKeys } from './entity-field-keys';
import { EntityFieldName } from './entity-field-name';
import { notEmpty } from '../../../utils/not-empty';

const Flex = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
`;

type Props = {
  // id: string;
  onRefChange: (instance: Textfield<any>) => void;
  onChange: (
    id: string,
    values: {
      name?: string;
      fillColor?: string;
      textColor?: string;
      lineColor?: string;
    },
  ) => void;
  onSubmitKeyUp: () => void;
  onDelete: (id: string | string[]) => () => void;
  fieldKeys: EntityFieldKeys;
  fieldName: EntityFieldName;
  fieldType: EntityFieldType;
  getById: (id: string) => UMLElement | null;
};

const EntityFieldUpdate = ({
  onRefChange,
  onChange,
  onSubmitKeyUp,
  onDelete,
  fieldKeys,
  fieldName,
  fieldType,
  getById,
}: Props) => {
  /*
  const children = element.ownedElements.map((childId) => getById(childId) as UMLElement).filter(notEmpty);
  const fieldKeys = children.find((child) => child instanceof EntityFieldKeys)!;
  const fieldType = children.find((child) => child instanceof EntityFieldType)!;
  */

  const [colorOpen, setColorOpen] = useState(false);

  const toggleColor = () => {
    setColorOpen(!colorOpen);
  };

  const handleNameChange = (newName: string) => {
    onChange(fieldName.id, { name: newName });
  };

  const handleTypeChange = (newFieldType: string) => {
    onChange(fieldType.id, { name: newFieldType });
  };

  const handleKeysChange = (newKeys: string) => {
    onChange(fieldKeys.id, { name: newKeys });
  };

  const handleDelete = () => {
    onDelete([fieldKeys.id, fieldName.id, fieldType.id])();
  };

  return (
    <>
      <Flex>
        <Textfield
          placeholder="Name"
          ref={onRefChange}
          gutter
          value={fieldName.name}
          onChange={handleNameChange}
          onSubmitKeyUp={onSubmitKeyUp}
        />
        <ColorButton onClick={toggleColor} />
        <Button color="link" tabIndex={-1} onClick={handleDelete}>
          <TrashIcon />
        </Button>
      </Flex>
      <Flex>
        <Textfield
          placeholder="Keys"
          ref={onRefChange}
          gutter
          value={fieldKeys.name}
          onChange={handleKeysChange}
          onSubmitKeyUp={onSubmitKeyUp}
        />
      </Flex>
      <Flex>
        <Textfield
          ref={onRefChange}
          placeholder="Type"
          value={fieldType.name}
          onChange={handleTypeChange}
          onSubmitKeyUp={onSubmitKeyUp}
        />
      </Flex>
      {/* TODO: style pane for each element, not just name */}
      <StylePane open={colorOpen} element={fieldName} onColorChange={onChange} fillColor textColor />
    </>
  );
};

export default EntityFieldUpdate;
