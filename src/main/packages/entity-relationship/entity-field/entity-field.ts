/**
 * Entity fields are split into three elements: EntityFieldKeys, EntityFieldName, and EntityFieldType.
 *
 * This is done because IUMLElements can essentially only store one value (UMLELement.name)
 * and we want to store three values.
 *
 * Nesting these elements under a single EntityField component makes the EntityComponent layout
 * difficult to manage. Instead, these elements are direct children to Entity in a single array.
 *
 * The first three elements (keys, name, and type, in that order) in the array correspond to the first field,
 * the next three elements correspond to the next field, and so on.
 */

import { EntityFieldKeys } from './entity-field-keys';
import { EntityFieldName } from './entity-field-name';
import { EntityFieldType } from './entity-field-type';

// Convenience exports so we don"t have to import from each file
// (tslint doesn"t allow multiple classes in one file)
export { EntityFieldKeys } from './entity-field-keys';
export { EntityFieldName } from './entity-field-name';
export { EntityFieldType } from './entity-field-type';

/**
 * All entity field element types. Used as the type for the Entity children array.
 */
export type EntityFieldElements = EntityFieldKeys | EntityFieldName | EntityFieldType;
