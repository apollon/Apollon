import { EntityRelationshipElementType } from '..';
import { UMLElementType } from '../../uml-element-type';
import { UMLClassifierMember } from '../../common/uml-classifier/uml-classifier-member';

export class EntityFieldName extends UMLClassifierMember {
  type: UMLElementType = EntityRelationshipElementType.EntityFieldName;
}
