import { EntityRelationshipElementType } from '..';
import { UMLClassifierMember } from '../../common/uml-classifier/uml-classifier-member';

export class EntityFieldType extends UMLClassifierMember {
  type = EntityRelationshipElementType.EntityFieldType;
}
