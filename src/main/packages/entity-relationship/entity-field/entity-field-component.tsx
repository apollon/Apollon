import React, { FunctionComponent } from 'react';
import { useSelector } from 'react-redux';
import { Text } from '../../../components/controls/text/text';
import { ModelState } from '../../../components/store/model-state';
import { EntityFieldElements } from '../entity-field/entity-field';
import { ThemedRect } from '../../../components/theme/themedComponents';

export const EntityFieldElementComponent: FunctionComponent<Props> = ({ element, scale, fillColor }) => {
  return (
    <g>
      <ThemedRect fillColor={fillColor || element.fillColor} strokeColor="black" width="100%" height="100%" />
      <Text fontWeight="normal">{element.name}</Text>
    </g>
  );
};

interface Props {
  element: EntityFieldElements;
  scale: number;
  fillColor?: string;
}
