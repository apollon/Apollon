import { EntityRelationshipElementType } from '..';
import { UMLElementType } from '../../uml-element-type';
import { IUMLElement } from '../../../services/uml-element/uml-element';
import { UMLClassifier } from '../../common/uml-classifier/uml-classifier';
import { ILayer } from '../../../services/layouter/layer';
import { ILayoutable } from '../../../services/layouter/layoutable';
import { EntityFieldElements } from '../entity-field/entity-field';
import { EntityFieldType } from '../entity-field/entity-field-type';
import { EntityFieldKeys } from '../entity-field/entity-field-keys';
import { EntityFieldName } from '../entity-field/entity-field-name';
import { DeepPartial } from 'redux';
import { assign } from '../../../utils/fx/assign';
import { Text } from '../../../utils/svg/text';

/**
 * Entity element in an entity-relationship diagram.
 * See comment in `../entity-field/entity-field.ts` for how children are stored.
 */
export class Entity extends UMLClassifier {
  type: UMLElementType = EntityRelationshipElementType.Entity;

  constructor(values?: DeepPartial<Entity>) {
    super();
    assign<Entity>(this, values);
  }

  reorderChildren(children: IUMLElement[]): string[] {
    // Children in the array are in this order: [FieldKeys, FieldName, FieldType, <repeat>]
    // hopefully don't need to worry about reordering this elements
    return children.map((element) => element.id);
  }

  static childrenAsNestedArray(children: EntityFieldElements[]): [EntityFieldKeys, EntityFieldName, EntityFieldType][] {
    const nested = [];
    for (let i = 0; i < children.length; i += 3) {
      nested.push(children.slice(i, i + 3) as [EntityFieldKeys, EntityFieldName, EntityFieldType]);
    }
    return nested;
  }

  // Prepare this and child elements to be rendered
  render(layer: ILayer, children: ILayoutable[] = []): ILayoutable[] {
    const fieldKeys = children.filter((x): x is EntityFieldKeys => x instanceof EntityFieldKeys);
    const fieldNames = children.filter((x): x is EntityFieldName => x instanceof EntityFieldName);
    const fieldTypes = children.filter((x): x is EntityFieldType => x instanceof EntityFieldType);

    if (!(fieldKeys.length === fieldNames.length && fieldNames.length === fieldTypes.length)) {
      throw new Error('EntityFieldElements invalid: unequal number keys/names/types');
    }

    // display fields as a grid

    // Get the previous width of this component by looking at the current column widths (they haven't been changed yet)
    let prevWidths = [fieldKeys[0].bounds.width, fieldNames[0].bounds.width, fieldTypes[0].bounds.width];
    let prevFullWidth = prevWidths[0] + prevWidths[1] + prevWidths[2];

    if (prevFullWidth === 0) {
      prevWidths = [30, 40, 30];
      prevFullWidth = 100;
    }

    // Calculate the width scale multiplier
    const widthScale = this.bounds.width / prevFullWidth;

    // Use the width scale to calculate the new column widths
    const newWidths = prevWidths.map((val) => val * widthScale);

    // If the width did not increase, determine minimum width of each column
    if (widthScale <= 1) {
      // If the width did not increase, calculate the new minimum widths
      const radix = 10;
      const minWidths = [fieldKeys, fieldNames, fieldTypes].map((elements) => {
        return elements.reduce((current, element) => {
          return Math.max(current, Math.round((Text.size(layer, element.name).width + 20) / radix) * radix);
        }, 0);
      });

      // column width relative to its minimum width
      let relativeWidths = newWidths.map((val, i) => val - minWidths[i]);

      // loop while a column width is below its minimum
      while (relativeWidths.find((val, i) => val < 0) !== undefined) {
        // filter widths
        const belowMin = [];
        const equalMin = [];
        const aboveMin = [];
        for (let i = 0; i < 3; i++) {
          if (relativeWidths[i] < 0) {
            belowMin.push(i);
          } else if (relativeWidths[i] === 0) {
            equalMin.push(i);
          } else {
            aboveMin.push(i);
          }
        }

        // set columns with width below min to min width
        for (const i of belowMin) {
          newWidths[i] = minWidths[i];
        }

        // scale to apply to columns above min
        const remainingScale = 1 - belowMin.reduce((prev, cur) => prev + cur) / this.bounds.width;
        for (const i of aboveMin) {
          newWidths[i] *= remainingScale;
        }

        relativeWidths = newWidths.map((val, i) => val - minWidths[i]);
      }
    }

    let y = this.headerHeight;

    for (let i = 0; i < fieldKeys.length; i++) {
      const keys = fieldKeys[i];
      keys.bounds.x = 0;
      keys.bounds.y = y;
      keys.bounds.width = newWidths[0];

      const name = fieldNames[i];
      name.bounds.x = keys.bounds.width;
      name.bounds.y = y;
      name.bounds.width = newWidths[1];

      const type = fieldTypes[i];
      type.bounds.x = name.bounds.x + name.bounds.width;
      type.bounds.y = y;
      type.bounds.width = newWidths[2];

      y += name.bounds.height;
    }

    this.bounds.width = Math.max(this.bounds.width, newWidths[0] + newWidths[1] + newWidths[2]);
    this.bounds.height = y;

    return [this, ...children];
  }
}
