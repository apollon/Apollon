import React, { Component, ComponentClass, createRef } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import styled from 'styled-components';
import { Button } from '../../../components/controls/button/button';
import { ColorButton } from '../../../components/controls/color-button/color-button';
import { Divider } from '../../../components/controls/divider/divider';
import { TrashIcon } from '../../../components/controls/icon/trash';
import { Textfield } from '../../../components/controls/textfield/textfield';
import { Header } from '../../../components/controls/typography/typography';
import { I18nContext } from '../../../components/i18n/i18n-context';
import { localized } from '../../../components/i18n/localized';
import { ModelState } from '../../../components/store/model-state';
import { StylePane } from '../../../components/style-pane/style-pane';
import { IUMLElement, UMLElement } from '../../../services/uml-element/uml-element';
import { UMLElementRepository } from '../../../services/uml-element/uml-element-repository';
import { AsyncDispatch } from '../../../utils/actions/actions';
import { notEmpty } from '../../../utils/not-empty';

import { Entity } from '../entity/entity';
import { EntityFieldElements } from '../entity-field/entity-field';
import { EntityFieldType } from '../entity-field/entity-field-type';
import { EntityFieldKeys } from '../entity-field/entity-field-keys';
import { EntityFieldName } from '../entity-field/entity-field-name';
import EntityFieldUpdate from '../entity-field/entity-field-update';

const Flex = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
`;

type State = {
  fieldToFocus?: Textfield<string> | null;
  colorOpen: boolean;
};

const getInitialState = (): State => ({
  fieldToFocus: undefined,
  colorOpen: false,
});

class EntityComponent extends Component<Props, State> {
  state = getInitialState();
  newFieldField = createRef<Textfield<string>>();

  private toggleColor = () => {
    this.setState((state) => ({
      colorOpen: !state.colorOpen,
    }));
  };

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<{}>, snapshot?: any) {
    if (this.state.fieldToFocus) {
      this.state.fieldToFocus.focus();
      this.setState({ fieldToFocus: undefined });
    }
  }

  render() {
    const { element, getById } = this.props;
    const children = element.ownedElements.map((id) => getById(id) as EntityFieldElements).filter(notEmpty);
    const fieldRefs: (Textfield<string> | null)[] = [];

    return (
      <div>
        <section>
          <Flex>
            <Textfield value={element.name} onChange={this.rename(element.id)} autoFocus />
            <ColorButton onClick={this.toggleColor} />
            <Button color="link" tabIndex={-1} onClick={this.delete(element.id)}>
              <TrashIcon />
            </Button>
          </Flex>
          <StylePane
            open={this.state.colorOpen}
            element={element}
            onColorChange={this.props.update}
            fillColor
            lineColor
            textColor
          />
          <Divider />
        </section>
        <section>
          <Header>Fields</Header>
          {Entity.childrenAsNestedArray(children).map(([fieldKeys, fieldName, fieldType], index: number) => (
            <>
              <EntityFieldUpdate
                key={fieldName.id}
                onChange={this.props.update}
                onSubmitKeyUp={() =>
                  index === children.length - 1
                    ? this.newFieldField.current?.focus()
                    : this.setState({ fieldToFocus: fieldRefs[index + 1] })
                }
                onDelete={this.delete}
                onRefChange={(ref) => (fieldRefs[index] = ref)}
                fieldKeys={fieldKeys}
                fieldName={fieldName}
                fieldType={fieldType}
                getById={getById}
              />
              <Divider />
            </>
          ))}
          <Textfield
            ref={this.newFieldField}
            outline
            value=""
            onSubmit={this.createField}
            onSubmitKeyUp={(_key, _value) => this.setState({ fieldToFocus: this.newFieldField.current })}
            onKeyDown={(event) => {
              // workaround when 'tab' key is pressed:
              // prevent default and execute blur manually without switching to next tab index
              // then set focus to newAttributeField field again (componentDidUpdate)
              if (event.key === 'Tab' && event.currentTarget.value) {
                event.preventDefault();
                event.currentTarget.blur();
                this.setState({
                  fieldToFocus: this.newFieldField.current,
                });
              }
            }}
          />
        </section>
      </div>
    );
  }

  /**
   * Create an entity field by creating its name, keys, and type elements
   */
  private createField = (value: string) => {
    const { element, create } = this.props;
    const fieldKeys = new EntityFieldKeys({ name: 'Keys' });
    const fieldName = new EntityFieldName({ name: value });
    const fieldType = new EntityFieldType({ name: 'Type' });
    create([fieldKeys, fieldName, fieldType], element.id);
  };

  private rename = (id: string) => (name: string) => {
    this.props.update(id, { name });
  };

  private delete = (id: string | string[]) => () => {
    this.props.delete(id);
  };
}

interface OwnProps {
  element: Entity;
}

type StateProps = {};

interface DispatchProps {
  create: typeof UMLElementRepository.create;
  update: typeof UMLElementRepository.update;
  delete: typeof UMLElementRepository.delete;
  getById: (id: string) => UMLElement | null;
}

type Props = OwnProps & StateProps & DispatchProps & I18nContext;

const enhance = compose<ComponentClass<OwnProps>>(
  localized,
  connect<StateProps, DispatchProps, OwnProps, ModelState>(null, {
    create: UMLElementRepository.create,
    update: UMLElementRepository.update,
    delete: UMLElementRepository.delete,
    getById: UMLElementRepository.getById as any as AsyncDispatch<typeof UMLElementRepository.getById>,
  }),
);

export const EntityUpdate = enhance(EntityComponent);
