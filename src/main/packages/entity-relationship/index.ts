// Establishes the element type of the entity in our entity relationship diagram
export const EntityRelationshipElementType = {
  Entity: 'Entity',
  EntityFieldName: 'EntityFieldName',
  EntityFieldKeys: 'EntityFieldKeys',
  EntityFieldType: 'EntityFieldType',
} as const;

// Establishes the baseline relationship type for the system to use when building Entity Relationship diagrams
export const EntityRelationshipType = {
  EntityRelationship: 'EntityRelationship',
} as const;

// Creates a constant that stores different types of endpoints available for Entity Relationships
// Sectioned off into different types, to allow for user to change endpoint types on their own, rather than for the entire line
export const EntityRelationshipEndpointType = {
  Default: 'Default',
  OneTo: 'OneTo',
  ZeroOrOneTo: 'ZeroOrOneTo',
  OneAndOnlyOneTo: 'OneAndOnlyOneTo',
  ManyTo: 'ManyTo',
  OneOrManyTo: 'OneOrManyTo',
  ZeroOrManyTo: 'ZeroOrManyTo',
} as const;

// Creates a type for our previously established types of endpoints
export type EntityRelationshipEndpointType = keyof typeof EntityRelationshipEndpointType;
